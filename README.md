# gitlab-api-demo

Demo the GitLab API.


## Establish project identifier

```bash
# Get the 'origin' remote URL or fall back to the first remote URL and extract the namespace
project_namespace=$(git remote get-url origin 2>/dev/null || git remote get-url $(git remote | head -n 1) | sed -E 's/^(git@[^:]+:|https:\/\/[^\/]+\/)(.+)\\.git$/\2/')

project=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_namespace}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --arg project_path "${project_namespace}" '.[] | select(.path_with_namespace == $project_namespace)')

project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_namespace}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --arg project_path "${project_namespace}" '.[] | select(.path_with_namespace == $project_namespace) | .id')
```

### Get jobs

[`https://docs.gitlab.com/ee/api/jobs.html`](https://docs.gitlab.com/ee/api/jobs.html)

```sh
curl --silent --show-error --location "https://gitlab.com/api/v4/projects/${project_id}/jobs" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq

curl --silent --show-error --location "https://gitlab.com/api/v4/projects/${project_id}/jobs?scope[]=pending&scope[]=running" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq
```

### Get pipeline schedule

```sh
curl --silent --show-error --location "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" > ./pipelines.json
pipeline_id=$(cat ./pipelines.json | jq --raw-output '.[].id')

if [ -z $pipeline_id]; then
```

### Create pipeline schedule

```sh
curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules" --form description="Balance shards [staging]" --form ref="main" --form cron="0 0 1 * *" --form cron_timezone="UTC" --form active="false" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" > ./pipeline.json
pipeline_id=$(cat ./pipeline.json | jq --raw-output '.id')
fi
```

### Create pipeline schedule variables

```sh
curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" --form "key=SCHEDULED_EXECUTION" --form "value=true" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq
```

## CI/CD variables

Configure the project CI/CD variables.

```sh
project_name=$(basename $(pwd))
project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_name}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.[].id')
curl --silent --show-error --location --ssl --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=CHEF_CLIENT_PRIVATE_KEY" --form "value=$(cat ./nelsnelson.pem)" | jq
curl --silent --show-error --location --ssl --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=GCLOUD_TERRAFORM_GSTG_PRIVATE_KEY" --form "value=$(cat ./gcloud_terraform_gstg_private_key.json)" | jq
curl --silent --show-error --location --ssl --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=GCLOUD_TERRAFORM_GPRD_PRIVATE_KEY" --form "value=$(cat ./gcloud_terraform_gprd_private_key.json)" | jq
```

## Clean up CI jobs and artifacts

Delete some artifacts and jobs.

```sh
project_name=$(basename $(pwd))
project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_name}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.[].id')
curl --silent --show-error --location --request DELETE "https://gitlab.com/api/v4/projects/${project_id}/artifacts" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq
jobs=$(curl --silent --show-error --location "https://gitlab.com/api/v4/projects/${project_id}/jobs" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.[].id')
for job_id in ${jobs[@]}; do
    curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}/erase" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq
done
```

Get a job log.

```sh
job_id="${CI_JOB_ID}"
project_name=$(basename $(pwd))
project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_name}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.[].id')
curl --silent --show-error --location --globoff "https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.artifacts[] | select(.file_type == "trace") | .filename'
curl --silent --show-error --location --globoff "https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.artifacts[] | select(.file_type == "trace") | .size'
```

Invoke a schedule.

```sh
schedule='Replace instances [gstg]'
project_name=$(basename $(pwd))
project_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/search?scope=projects&search=${project_name}" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output '.[].id')
schedule_id=$(curl --silent --show-error --location "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq --raw-output --arg schedule "${schedule}" '.[] | select(.description == $schedule) | .id')
curl --silent --show-error --location --request POST "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${schedule_id}/play" --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" | jq
```
